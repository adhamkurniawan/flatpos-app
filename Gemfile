source 'https://rubygems.org'

git_source(:github) do |repo_name|
  repo_name = "#{repo_name}/#{repo_name}" unless repo_name.include?("/")
  "https://github.com/#{repo_name}.git"
end

# Used for functionality
gem 'rails', '~> 5.1.3'
gem 'sqlite3'
gem 'puma', '~> 3.7'
gem 'devise'
gem 'paperclip'
gem 'simple_form'
gem 'cancan'
gem 'omniauth'
gem 'omniauth-facebook'
gem 'friendly_id'
gem 'country_select'
gem 'countries'

# Used for asset pipeline
gem 'sass-rails', '~> 5.0'
gem 'uglifier', '>= 1.3.0'
gem 'coffee-rails', '~> 4.2'
gem 'turbolinks', '~> 5'
gem 'jbuilder', '~> 2.5'
gem 'jquery-rails'
gem 'bootstrap-sass', '~> 3.3', '>= 3.3.4.1'
gem 'font-awesome-rails', '~> 4.7', '>= 4.7.0.2'
gem 'toastr-rails'
gem 'momentjs-rails', '~> 2.9',  :github => 'derekprior/momentjs-rails'
gem 'datetimepicker-rails', github: 'zpaulovics/datetimepicker-rails', branch: 'master', submodules: true
gem 'will_paginate', '~> 3.1', '>= 3.1.6'
gem 'jquery-datatables-rails', '~> 3.4.0'

# Used for development and test
group :development, :test do
  gem 'byebug', platforms: [:mri, :mingw, :x64_mingw]
  gem 'capybara', '~> 2.13'
  gem 'selenium-webdriver'
end

# Used only for development
group :development do
  gem 'web-console', '>= 3.3.0'
  gem 'listen', '>= 3.0.5', '< 3.2'
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
end

# Used to know version our application
gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]
