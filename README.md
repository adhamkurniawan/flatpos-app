# FlatPos Application

Short Description:

The goal of this project to help people who have business in selling and buying (Point Of Sale) for their product and as my practice in Ruby on Rails.

Features:

* Tracking product
* Receiving product from supplier
* Selling product to customer
* Up to date information
* Total receiving profit

##### Note: I'm not already finish this application because I make this application as my practice Ruby on Rails each weekend after my work. I apologize for the application not in full feature. But, hopefully you can review the code I create and give me some feedback for me to improve my skills.
