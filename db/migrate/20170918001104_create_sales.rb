class CreateSales < ActiveRecord::Migration[5.1]
  def change
    create_table :sales do |t|
      t.references :customer, foreign_key: true
      t.references :user, foreign_key: true
      t.references :payment_method, foreign_key: true
      t.references :payment_status, foreign_key: true
      t.references :product, foreign_key: true
      t.decimal :sell_cost
      t.integer :quantity

      t.timestamps
    end
  end
end
