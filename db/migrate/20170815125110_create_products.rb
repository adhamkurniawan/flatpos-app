class CreateProducts < ActiveRecord::Migration[5.1]
  def change
    create_table :products do |t|
      t.references :category, foreign_key: true
      t.references :unit, foreign_key: true
      t.string :code_product
      t.string :name
      t.string :size
      t.text :description
      t.decimal :buy_cost
      t.decimal :sell_cost
      t.integer :quantity

      t.timestamps
    end
  end
end
