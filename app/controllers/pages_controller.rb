class PagesController < ApplicationController
  skip_before_action :authenticate_user!, only: [:index]
  layout 'app-landing'

  def index
  end
end
