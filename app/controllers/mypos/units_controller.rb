class Mypos::UnitsController < ApplicationController
  before_action :find_unit, except: [:index, :new, :create]

  def index
    @units = Unit.where(user_id: current_user).paginate(:page => params[:page], :per_page => 15)
  end

  def show
  end

  def new
    @unit = current_user.units.build
  end

  def create
    @unit = current_user.units.build(unit_params)

    if @unit.save
      flash[:notice] = "Successfully added unit"
      redirect_to mypos_units_path
    else
      flash[:alert] = "Something went wrong"
      render "new"
    end
  end

  def edit
  end

  def update
    if @unit.update(unit_params)
      flash[:notice] = "Successfully updated unit"
      redirect_to mypos_unit_path(@unit)
    else
      flash[:alert] = "Something went wrong"
      render "edit"
    end
  end

  def destroy
    @unit.destroy
    flash[:notice] = "Successfully deleted unit"
    redirect_to mypos_units_path
  end

  private
    def find_unit
      @unit = Unit.find(params[:id])
    end

    def unit_params
      params.require(:unit).permit(:name)
    end
end
