class Mypos::ProductSellingsController < ApplicationController
  before_action :find_product_selling

  def index
  end

  def show
  end

  def new
  end

  def create
  end

  def edit
  end

  def update
  end

  def destroy
  end

  private
    def product_selling_params
    end

    def find_product_selling
    end
end
