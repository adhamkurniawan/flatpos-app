class Mypos::ApplicationController < ActionController::Base
  helper: application
  protect_from_forgery with: :exception
  # check_authorization

  # rescue_from CanCan::AccessDenied do
  #   redirect_to root_path, alert: "Access Denied"
  # end
  before_action :authenticate_user!
end
