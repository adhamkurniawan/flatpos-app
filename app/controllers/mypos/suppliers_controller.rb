class Mypos::SuppliersController < ApplicationController
  before_action :find_supplier, except: [:index, :new, :create]

  def index
    @suppliers = Supplier.where(user_id: current_user).paginate(:page => params[:page], :per_page => 15)
  end

  def show
  end

  def new
    @supplier = current_user.suppliers.build
  end

  def create
    @supplier = current_user.suppliers.build(supplier_params)

    if @supplier.save
      flash[:notice] = "Successfully added supplier"
      redirect_to mypos_suppliers_path
    else
      flash[:alert] = "Something went wrong"
      render "new"
    end
  end

  def edit
  end

  def update
    if @supplier.update(supplier_params)
      flash[:notice] = "Successfully updated supplier"
      redirect_to mypos_supplier_path(@supplier)
    else
      flash[:alert] = "Something went wrong"
      render "edit"
    end
  end

  def destroy
    @supplier.destroy
    flash[:notice] = "Successfully deleted supplier"
    redirect_to mypos_suppliers_path
  end

  private
    def find_supplier
      @supplier = Supplier.find(params[:id])
    end

    def supplier_params
      params.require(:supplier).permit(:company_name, :name, :email, :phone_number, :website, :address, :city, :state, :zip, :country, :comment)
    end
end
