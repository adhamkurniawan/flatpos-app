class Mypos::SalesController < ApplicationController
  before_action :find_sale, except: [:index, :new, :create]

  def index
    @sales = Sale.where(user_id: current_user).paginate(:page => params[:page], :per_page => 15)
  end

  def show
  end

  def new
    @sale = Sale.new
  end

  def create
    @sale = current_user.sales.build(sale_params)

    if @sale.save
      flash[:notice] = "Successfully added sale"
      redirect_to mypos_sales_path
    else
      flash[:alert] = "Something went wrong"
      render "new"
    end
  end

  def edit
  end

  def update
    if @sale.update(sale_params)
      flash[:notice] = "Successfully updated sale"
      redirect_to mypos_sale_path(@sale)
    else
      flash[:alert] = "Something went wrong"
      render "edit"
    end
  end

  def destroy
    @sale.destroy
    flash[:notice] = "Successfully deleted sale"
    redirect_to mypos_sales_path
  end

  private
    def sale_params
      params.require(:sale).permit(:customer_id, :payment_method_id, :payment_status_id, :product_id, :sell_cost, :quantity)
    end

    def find_sale
      @sale = Sale.find(params[:id])
    end
end
