class Mypos::ProductsController < ApplicationController
  before_action :find_product, except: [:index, :new, :create]

  def index
    @products = Product.where(user_id: current_user).paginate(:page => params[:page], :per_page => 15)
  end

  def show
  end

  def new
    @product = current_user.products.build
  end

  def create
    @product = current_user.products.build(product_params)

    if @product.save
      flash[:notice] = "Successfully added product"
      redirect_to mypos_products_path
    else
      flash[:alert] = "Something went wrong"
      render "new"
    end
  end

  def edit
  end

  def update
    if @product.update(product_params)
      flash[:notice] = "Successfully updated product"
      redirect_to mypos_product_path(@product)
    else
      flash[:alert] = "Something went wrong"
      render "edit"
    end
  end

  def destroy
    @product.destroy
    flash[:notice] = "Successfully deleted product"
    redirect_to mypos_products_path
  end

  private
    def find_product
      @product = Product.find(params[:id])
    end

    def product_params
      params.require(:product).permit(:category_id, :unit_id, :code_product, :name, :size, :description, :buy_cost, :sell_cost, :quantity)
    end
end
