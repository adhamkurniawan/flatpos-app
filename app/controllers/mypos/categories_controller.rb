class Mypos::CategoriesController < ApplicationController
  before_action :find_category, except: [:index, :new, :create]

  def index
    @categories = Category.where(user_id: current_user).paginate(:page => params[:page], :per_page => 15)
  end

  def show
  end

  def new
    @category = current_user.categories.build
  end

  def create
    @category = current_user.categories.build(category_params)

    if @category.save
      flash[:notice] = "Successfully added category"
      redirect_to mypos_categories_path
    else
      flash[:alert] = "Something went wrong"
      render "new"
    end
  end

  def edit
  end

  def update
    if @category.update(category_params)
      flash[:notice] = "Successfully updated category"
      redirect_to mypos_category_path(@category)
    else
      flash[:alert] = "Something went wrong"
      render "edit"
    end
  end

  def destroy
    @category.destroy
    flash[:notice] = "Successfully deleted category"
    redirect_to mypos_categories_path
  end

  private
    def find_category
      @category = Category.find(params[:id])
    end

    def category_params
      params.require(:category).permit(:name)
    end
end
