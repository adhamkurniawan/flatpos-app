class Mypos::CustomersController < ApplicationController
  before_action :find_customer, except: [:index, :new, :create]

  def index
    @customers = Customer.where(user_id: current_user).paginate(:page => params[:page], :per_page => 15)
  end

  def show
  end

  def new
    @customer = current_user.customers.build
  end

  def create
    @customer = current_user.customers.build(customer_params)

    if @customer.save
      flash[:notice] = "Successfully added customer"
      redirect_to mypos_customers_path
    else
      flash[:alert] = "Something went wrong"
      render "new"
    end
  end

  def edit
  end

  def update
    if @customer.update(customer_params)
      flash[:notice] = "Successfully updated customer"
      redirect_to mypos_customer_path(@customer)
    else
      flash[:alert] = "Something went wrong"
      render "edit"
    end
  end

  def destroy
    @customer.destroy
    flash[:notice] = "Successfully deleted customer"
    redirect_to mypos_customers_path
  end

  private
    def find_customer
      @customer = Customer.find(params[:id])
    end

    def customer_params
      params.require(:customer).permit(:company_name, :name, :email, :phone_number, :website, :address, :city, :state, :zip, :country)
    end
end
