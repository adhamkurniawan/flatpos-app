// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require bootstrap-sprockets
//= require dataTables/jquery.dataTables
//= require dataTables/bootstrap/3/jquery.dataTables.bootstrap

//= require moment
//= require bootstrap-datetimepicker
//= require pickers

//= require toastr

//= require dashboards/modernizr.custom
//= require dashboards/wow.min
//= require dashboards/Chart
//= require dashboards/underscore-min
//= require dashboards/clndr
//= require dashboards/site
//= require dashboards/jquery.circlechart
//= require dashboards/jquery.nicescroll
//= require dashboards/metisMenu.min
//= require dashboards/scripts
//= require dashboards/custom
