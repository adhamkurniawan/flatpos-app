class Supplier < ApplicationRecord
  belongs_to :user

  def country_name
    nation = ISO3166::Country[country]
    nation.translations[I18n.locale.to_s] || country.name
  end
end
