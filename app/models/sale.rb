class Sale < ApplicationRecord
  belongs_to :customer
  belongs_to :user
  belongs_to :payment_method
  belongs_to :payment_status
  belongs_to :product

  def total_cost
    self.sell_cost * self.quantity
  end
end
