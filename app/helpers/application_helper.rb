module ApplicationHelper
  def avatar_url(user)
    gravatar_id = Digest::MD5::hexdigest(current_user.email).downcase
    "https://gravatar.com/avatar/#{gravatar_id}.jpg?id=identicon&s=50"
  end

  def show_time
    time = Time.now.to_formatted_s(:long)
  end

  def company_name
    if current_user.company_name.blank?
      return "Your Company Name - Customers "
    else
      return current_user.company_name + " - Customers "
    end
  end
end
