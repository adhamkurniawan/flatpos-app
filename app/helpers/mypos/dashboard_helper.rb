module Mypos::DashboardHelper
  def checking_profile
    current_user.company_name.blank?
    current_user.date_of_birth.blank?
    current_user.address.blank?
    current_user.city.blank?
    current_user.province.blank?
    current_user.zip.blank?
    current_user.country.blank?
  end
end
