Rails.application.routes.draw do
  root to: "pages#index"

  namespace :mypos do
    get "dashboard", to: "dashboard#index"
    root to: "dashboard#index"

    resources :customers, :suppliers, :categories, :units, :products, :sales, :product_sellings
  end

  devise_for :users, path: '', path_names: { sign_in: 'login', sign_up: 'register', sign_out: 'logout' }
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
